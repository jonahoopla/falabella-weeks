var init = (function () {


	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
		group5 = new TimelineMax({paused:true});
		group6 = new TimelineMax({paused:true});
		group7 = new TimelineMax({paused:true});
		group8 = new TimelineMax({paused:true});
		group9 = new TimelineMax({paused:true});
		group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
		
	var player;

	//INICIAR ANIMACIONES

	function iniciar(tab) {
		//Destruimos la animación para mobile
		var isMobile = window.matchMedia("screen and (max-width: 760px)");

		switch(tab) {
			case "home":
				new Waypoint({
					element: document.getElementsByClassName('waypoint-1'),
					handler: function(direction) {
						if(direction === 'right'){
							group1.play();
							console.log(direction);
						}else if (direction === 'left'){
							group1.reverse();
							console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '30%'

				});
				
				break;

			case "episode_01":
				//var waypoint = new Waypoint({
				new Waypoint({
					element: document.getElementsByClassName('waypoint-1'),
					handler: function(direction) {
						if(direction === 'right'){
							group11.play();
							// console.log(direction);
						}else if (direction === 'left'){
							group11.reverse();
							// console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '70%'

				});
				break;
			
		}
	}
	

	//ANIMANDO
	/*ANIMACION DEL HOME*/
	group1
		.from(".slider .container span.h1", 1, {opacity:0, y:-20, ease:Back.easeOut}, 0.1)
		.from(".slider .container span.h2", 1, {opacity:0, y:20, ease:Back.easeOut}, 0.5)
		.from(".slider .container .frase", 1, {opacity:0, x:-20, ease:Back.easeOut}, 1)
		.from(".slider .container .button-more", 1, {opacity:0, y:50, ease:Back.easeOut}, 1.5)
		.from(".slider .container ul .one", 0.5, {opacity:0, y:10, ease:Back.easeOut}, 2)
		.from(".slider .container ul .two", 0.5, {opacity:0, y:10, ease:Back.easeOut}, 2.3)
		.from(".slider .container ul .three", 0.5, {opacity:0, y:10, ease:Back.easeOut}, 2.6)

	/*ANIMACION DEL EPISODIO*/
	
	group11
		.from(".scroll-serie .p02", 10, {x:200}, 1)
		.to(".scroll-serie .p02", 10, {x:-100}, 5)

		.from(".scroll-serie .p03", 7, {x:100}, 1.5)

		.from(".scroll-serie .p04", 7, {x:200}, 2)
		.to(".scroll-serie .p04", 7, {x:-100}, 7)

		.from(".scroll-serie .p04_1", 7, {x:200}, 2)
		.to(".scroll-serie .p04_1", 7, {x:-100}, 7)

		.from(".scroll-serie .p05", 7, {x:100}, 3)
		.from(".scroll-serie .p06", 7, {x:100}, 4)
		
		.from(".scroll-serie .p07_1", 7, {x:200}, 4)
		.to(".scroll-serie .p07_1", 7, {x:100}, 9)
		
		.from(".scroll-serie .p07_2", 7, {x:200}, 4)
		.to(".scroll-serie .p07_2", 7, {x:100}, 9)
		
		.from(".scroll-serie .p08", 7, {x:100}, 6)
	


	//INICIAMOS LA ANIMACION POR DEFAUL
	iniciar('home');

	//END


	$('.slider .button-more a').on('click', function(e){
		e.preventDefault();
		// $('.slider .container').fadeOut('slow');
		// $('.scroll-serie').fadeIn('slow');

		Waypoint.destroyAll();

		// group11.pause(0);
		// iniciar('episode_01');

	})



	
	var $slider = $(".landing-wrapper");
	var $content = $(".landing-inner-content");
	var mytween;

	
	var tl = new TimelineMax({paused: true});
	
	$('.scroll-serie .arrow img').on('click', function(){

		// Waypoint.destroyAll();
		// group11.pause(0);

		setTimeout(function(){ $('.scroll-serie .arrow').fadeOut('slow'); }, 2000);
	})

		// scroll left and right
		$content.on("mousemove", function(e) {
			if (e.clientX > $slider.width() / 2) {
				mytween = TweenMax.to($slider, 8, {
				scrollTo: { x: "+=250"}, ease: Power2.easeOut
				});
				
				group11.play();

			} else {
				mytween = TweenMax.to($slider, 8, {scrollTo: {x: "-=250"}, ease: Power2.easeOut});

				group11.reverse();
			}
		});	
	
	
	
	




})();